notmuch-tags
============

A simple major mode to edit notmuch batch files.

![Screenshot](data/screenshot.png)
