;;;; notmuch-tags.el -- Major mode for editing notmuch batch files  -*- lexical-binding: t; -*-
;; Copyright (C) 2022  Gergely Nagy

;; Author: Gergely Nagy
;; Version: 0.1
;; URL: https://git.madhouse-project.org/algernon/notmuch-tags.el

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, version 3.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Simple major mode for editing notmuch batch files. Syntax highlighting only
;; for now.

;;; Code:

(defface notmuch-tags/search-term-prefix-face
  '((t :inherit font-lock-builtin-face
       :weight bold))
  "Face used for notmuch search term prefixes."
  :group 'notmuch-tags)

(defface notmuch-tags/search-term-string-face
  '((t :inherit font-lock-string-face))
  "Face used for notmuch search term strings."
  :group 'notmuch-tags)

(defface notmuch-tags/tag-add-face
  '((t :inherit font-lock-variable-name-face
       :extend t
       :underline t))
  "Face used for notmuch +tags."
  :group 'notmuch-tags)

(defface notmuch-tags/tag-remove-face
  '((t :inherit font-lock-variable-name-face
       :extend t
       :strike-through t))
  "Face used for notmuch -tags."
  :group 'notmuch-tags)

(defvar notmuch-tags-face-alist
  '((search-term-prefix notmuch-tags/search-term-prefix-face)
    (search-term-string notmuch-tags/search-term-string-face)
    (tag-add notmuch-tags/tag-add-face)
    (tag-remove notmuch-tags/tag-remove-face)
    (comment font-lock-comment-face)
    (operator font-lock-keyword-face)))

(defconst notmuch-tags-font-lock--operators-base
  '("and"
    "or"
    "not"
    "xor"))

(defconst notmuch-tags-font-lock--operators-with-threshold
  '("near"
    "adj"))

(defconst notmuch-tags-font-lock--operators
  (list
   (rx-to-string `(: symbol-start
                     (or ,@notmuch-tags-font-lock--operators-base
                         (group
                          (or ,@notmuch-tags-font-lock--operators-with-threshold)
                          (* (group "/" (1+ digit)))))
                     symbol-end))

   '(0 (alist-get 'operator notmuch-tags-face-alist))))

(defconst notmuch-tags-font-lock--search-terms
  (list
   (rx (: symbol-start
          (group-n 1 (1+ word)) ":"
          (group-n 2 (or (1+ (not whitespace))
                         (group "\"" (1+ any) "\"")))
          symbol-end))

   '(1 (alist-get 'search-term-prefix notmuch-tags-face-alist))
   '(2 (alist-get 'search-term-string notmuch-tags-face-alist) nil t)))

(defconst notmuch-tags-font-lock--comments
  (list
   (rx "#" (0+ not-newline) eol)

   '(0 (alist-get 'comment notmuch-tags-face-alist))))

(defconst notmuch-tags-font-lock--tag-add
  (list
   (rx (: symbol-start
          "+"
          (1+ (or word punctuation))
          symbol-end))

   '(0 (alist-get 'tag-add notmuch-tags-face-alist))))

(defconst notmuch-tags-font-lock--tag-remove
  (list
   (rx (: symbol-start
          "-"
          (1+ (or word punctuation))
          symbol-end))

   '(0 (alist-get 'tag-remove notmuch-tags-face-alist))))

(defconst notmuch-tags-font-lock
  (list
   notmuch-tags-font-lock--comments
   notmuch-tags-font-lock--tag-add
   notmuch-tags-font-lock--tag-remove
   notmuch-tags-font-lock--operators
   notmuch-tags-font-lock--search-terms))

;;;###autoload
(define-derived-mode notmuch-tags-mode fundamental-mode "notmuch-tags"
  "Major mode for editing notmuch batch files"
  (setq font-lock-defaults '(notmuch-tags-font-lock)))

(provide 'notmuch-tags)

;;; notmuch-tags.el ends here
